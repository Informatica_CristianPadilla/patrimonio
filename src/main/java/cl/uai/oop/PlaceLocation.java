/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uai.oop;

import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.record.Location;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.InetAddress;

/**
 *
 * @author gohucan
 */
public final class PlaceLocation implements Serializable {

    private Double latitude;
    private Double longitude;

    public PlaceLocation(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return String.format("%s;%s", latitude, longitude);
    }
    
    public static PlaceLocation currentLocation() throws IOException, GeoIp2Exception {
        String dbLocation = "GeoLite2-City.mmdb";

        ClassLoader classLoader = PlaceLocation.class.getClassLoader();
        File database = new File(classLoader.getResource(dbLocation).getPath());
        DatabaseReader dbReader = new DatabaseReader.Builder(database)
                .build();

        InetAddress ipAddress = InetAddress.getLocalHost();
        CityResponse response = dbReader.city(ipAddress);
        Location l = response.getLocation();
        return new PlaceLocation(l.getLatitude(), l.getLongitude());
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uai.oop.server;

import cl.uai.oop.PlaceLocation;
import cl.uai.oop.client.Place;
import cl.uai.oop.Utils;
import java.util.Comparator;

/**
 *
 * @author ppadilla
 */
public class Comparador implements Comparator<Place>{
private final PlaceLocation ob;

    public Comparador(PlaceLocation oj) {
       ob=oj; 
    }


    @Override
    public int compare(Place o1, Place o2) {
        Utils u = new Utils();
      int i = (int) u.distance(ob,o1.getUbicacion());
      
      int c = (int) u.distance(ob,o2.getUbicacion());
        
        return (i-c);
        
    }
    
}

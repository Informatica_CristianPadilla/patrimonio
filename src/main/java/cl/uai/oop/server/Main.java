/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uai.oop.server;

import cl.uai.oop.PlaceLocation;
import cl.uai.oop.client.Place;
import static com.sun.glass.ui.Application.run;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author gohucan
 */
public class Main extends Thread {   
public Socket clientSocket;
private static List<Place> lugares = new ArrayList<>();

    public Main(Socket clientSocket) {
        this.clientSocket = clientSocket;
    }

    @Override
    public void run() {
        try {
                    InputStream is = clientSocket.getInputStream();
                    OutputStream os = clientSocket.getOutputStream();
                    DataInputStream in = new DataInputStream(is);
                    DataOutputStream out = new DataOutputStream(os);
                    String command = in.readUTF();
                    String[] parameters = command.split(";");
                    int option = Integer.parseInt(parameters[0]);
                    if (option == 1) {
                        int c=1;
                        PlaceLocation pl = new PlaceLocation(Double.parseDouble(parameters[1]), Double.parseDouble(parameters[2]));
                       try {
                        Collections.sort(lugares,new Comparador(pl));
                         }catch (Exception e) {
                    e.printStackTrace();
                        c=0;
                        }
                       ObjectOutputStream dos = new ObjectOutputStream(os);
                       if(c!=0) dos.writeObject(lugares);
                       else dos.writeObject("Error en ordenamiento");
                    } else if (option == 2) {
                         int c=1;
                        String placeName = parameters[1];
                        Place p = null;
                        try {
                             p=lugares.get(lugares.indexOf(placeName));
                        }catch (Exception e) 
                        {
                            c=0;
                    e.printStackTrace();
                        }
                        
                        ObjectOutputStream dos = new ObjectOutputStream(os);
                       if(c!=0) dos.writeObject(p);
                       else dos.writeObject("Eror al encontrar lugar");
                    }
    }
     catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
    }
    
    public static void main(String args[]) {
        try {
            int portNumber = Integer.parseInt(args[0]);
            ServerSocket serverSocket = new ServerSocket(portNumber);
            while (true) {
                Socket clientSocket = null;
                try {
                    clientSocket = serverSocket.accept();
                   Thread t = new Main(clientSocket);
                    t.start();
                    
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}

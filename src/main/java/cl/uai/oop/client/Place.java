/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uai.oop.client;

import cl.uai.oop.PlaceLocation;
import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

/**
 *
 * @author ppadilla
 */
public class Place implements Serializable {
   private String nombre;
   private String descripcion;
   private TipoLugar tipo;
   private PlaceLocation ubicacion;
   private Date apertura;
   private Date cierre;

    public Place(String nombre, String descripcion, TipoLugar tipo, PlaceLocation ubicacion, Date apertura, Date cierre) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.tipo = tipo;
        this.ubicacion = ubicacion;
        this.apertura = apertura;
        this.cierre = cierre;
    }

    
   @Override
    public boolean equals(Object Name)
    {
        if(Name instanceof String) if(((String) Name).equals(this.nombre)) return true;
        if(Name instanceof Place) if(((Place) Name).getNombre().equals(this.nombre)) return true;
     
        return false;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setTipo(TipoLugar tipo) {
        this.tipo = tipo;
    }

    public void setUbicacion(PlaceLocation ubicacion) {
        this.ubicacion = ubicacion;
    }

    public void setApertura(Date apertura) {
        this.apertura = apertura;
    }

    public void setCierre(Date cierre) {
        this.cierre = cierre;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public TipoLugar getTipo() {
        return tipo;
    }

    public PlaceLocation getUbicacion() {
        return ubicacion;
    }

    public Date getApertura() {
        return apertura;
    }

    public Date getCierre() {
        return cierre;
    }
    
    
}

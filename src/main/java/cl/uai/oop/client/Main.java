/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uai.oop.client;

import cl.uai.oop.PlaceLocation;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author gohucan
 */
public class Main {

    public static void main(String[] args) throws ClassNotFoundException {
        try {
            String hostName = args[0];
            int portNumber = Integer.parseInt(args[1]);
            Socket mySocket = new Socket(hostName, portNumber);
            InputStream is = mySocket.getInputStream();
            OutputStream os = mySocket.getOutputStream();
            Scanner myScanner = new Scanner(System.in);
            int option = -1;
            
            while(true) {
                System.out.println("Bienvenido. Ingrese la opción deseada.");
                System.out.println("1 para listar los lugares cercanos");
                System.out.println("2 para encontrar información de un lugar");
                System.out.println("0 para salir");
                option = myScanner.nextInt();
                if (option == 0)
                    break;
                else if (option == 1) {
                    PlaceLocation pl = PlaceLocation.currentLocation();
                    DataOutputStream dos = new DataOutputStream(os);
                    dos.writeUTF(String.format("1;%s", pl.toString()));
                    dos.flush();
                    ObjectInputStream in = new ObjectInputStream(mySocket.getInputStream());
                    if(in.readObject() instanceof Place){
                    List<Place> lugares =(List<Place>) in.readObject();
                    for(int i =0;i<lugares.size();i++)
                    System.out.println(lugares.get(i));}
                    else System.out.println(((String )in.readObject()));// MENSAJE ERROR
                } else if (option == 2) {
                        System.out.println("Ingrese Nombre del lugar\n");
                        String nombre = myScanner.nextLine();
                        DataOutputStream dos = new DataOutputStream(os);
                        dos.writeUTF(nombre);
                        ObjectInputStream in = new ObjectInputStream(mySocket.getInputStream());
                     if(in.readObject() instanceof Place) System.out.println(((Place) in.readObject()).getDescripcion());                        
                     else System.out.println(((String) in.readObject()));
                } else {
                    System.out.println("Opción inválida");
                }
            }
            
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (GeoIp2Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
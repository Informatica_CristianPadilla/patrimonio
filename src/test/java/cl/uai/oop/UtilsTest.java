/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uai.oop;

import com.github.javafaker.Faker;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author gohucan
 */
public class UtilsTest {
    
    private Faker faker = new Faker();
    private List<PlaceLocation> places = new ArrayList<>(100);
    
    public UtilsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of distance method, of class Utils.
     */
    @Test
    public void testDistance() {
        for(int j=0;j<99;j++){
        for(int i=0; i<99; i++) {
            
          if(i!=j)  assertTrue(Utils.distance(places.get(i), places.get(j))>0);
          else  assertTrue(Utils.distance(places.get(i), places.get(j))==0);
                    
        }
        
    }
    }
    
}
